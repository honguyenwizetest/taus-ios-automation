package pages.utils;

import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class UtilMethods {


    public static boolean swipeFromUpToBottom(IOSDriver driver)
    {
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            HashMap<String, String> scrollObject = new HashMap<String, String>();
            scrollObject.put("direction", "down");
            js.executeScript("mobile: scroll", scrollObject);
            System.out.println("Swipe down was Successfully done");
        }
        catch (Exception e)
        {
            System.out.println("swipe down was not successful");
        }
        return false;
    }

    public static void tapElement(IOSDriver driver, WebElement el){
        new TouchAction(driver).tap(ElementOption.element(el)).perform();
    }

    public static void swipeFromObject2OtherObject(IOSDriver driver, WebElement elFrom, WebElement elTo)
    {
        new TouchAction(driver).press(ElementOption.element(elFrom))
            .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(30))).moveTo(ElementOption.element(elTo)).release().perform();
    }

    public static boolean isElementClickable(IOSDriver driver, WebElement el, int timeOutInSeconds){
        if(UtilMethods.waitForElementClickable(driver, el,timeOutInSeconds)!=null){
            return true;
        } else {
            return false;
        }
    }


    public static boolean isElementVisible(IOSDriver driver, WebElement el, int timeOutInSeconds){
        if(UtilMethods.waitForElementVisible(driver, el,timeOutInSeconds)!=null){
            return true;
        } else {
            return false;
        }
    }

    public static WebElement waitForElementClickable(IOSDriver driver, final WebElement el, int timeOutInSeconds) {

        WebElement element;

        try {
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS.SECONDS); //nullify implicitlyWait()

            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
            element = wait.until(ExpectedConditions.elementToBeClickable(el));

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); //reset implicitlyWait
            return element; //return the element

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static WebElement waitForElementClickable(IOSDriver driver, final By byel, int timeOutInSeconds) {

        WebElement element;

        try {
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS.SECONDS); //nullify implicitlyWait()

            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
            element = wait.until(ExpectedConditions.elementToBeClickable(byel));

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); //reset implicitlyWait
            return element; //return the element

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static WebElement waitForElementVisible(IOSDriver driver, final WebElement el, int timeOutInSeconds) {

        WebElement element;

        try {
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS.SECONDS); //nullify implicitlyWait()

            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
            element = wait.until(ExpectedConditions.visibilityOf(el));

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); //reset implicitlyWait
            return element; //return the element

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static WebElement waitForElementPresent(IOSDriver driver, final By by, int timeOutInSeconds) {

        WebElement element;

        try {
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS.SECONDS); //nullify implicitlyWait()

            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
            element = wait.until(ExpectedConditions.presenceOfElementLocated(by));

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); //reset implicitlyWait
            return element; //return the element

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }





}
