package pages;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class SettingHelpPage extends BasePage {

    public SettingHelpPage(IOSDriver driver) {
        super(driver);
    }

    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='iOS']")
    private WebElement banner;

    @iOSFindBy(xpath = "//XCUIElementTypeOther[@name='search']")
    private WebElement searchField;

    public String getBanner(){
        return banner.getAttribute("label");
    }

    public boolean checkSearchField(){
        if(!searchField.findElement(By.xpath(".//XCUIElementTypeOther[@name='Search for:']")).isDisplayed()){
            return false;
        }

        if(!searchField.findElement(By.xpath(".//XCUIElementTypeSearchField")).isDisplayed()){
            return false;
        }

        if(!searchField.findElement(By.xpath(".//XCUIElementTypeButton[contains(@name,'Search')]")).isDisplayed()){
            return false;
        }

        return true;
    }

    public ArrayList<String> getHelpQuestions(){
        ArrayList<String> questions = new ArrayList<>();
        List<WebElement> helpQuestions = iosDriver.findElements(
                By.xpath("//XCUIElementTypeOther[@name='search']/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeLink/XCUIElementTypeStaticText"));

        for(WebElement question: helpQuestions){
            questions.add(question.getText());
        }

        return questions;
    }

}
