package pages;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

public class SubscribePage extends BasePage {
    public SubscribePage(IOSDriver driver){
        super(driver);
    }

    @iOSFindBy(accessibility = "LOG IN")
    private WebElement btnLogin;

    @iOSFindBy(accessibility = "SUBSCRIBE NOW")
    private WebElement btnSubscribe;

    @iOSFindBy(xpath = "//XCUIElementTypeTextView")
    private WebElement txtView;

    @iOSFindBy(accessibility = "Restore iTunes subscription")
    private WebElement btnRestoreiTune;

    @iOSFindBy(accessibility = "preferences.news.com.au")
    private WebElement linkPreference;

    public boolean isSubscribeTextExisting(String subscribeText){
        return iosDriver.findElementByAccessibilityId(subscribeText).getText().equals(subscribeText);
    }

    public WebElement getBtnLogin(){
        return btnLogin;
    }

    public WebElement getBtnSubscribe(){
        return btnSubscribe;
    }

    public WebElement getBtnRestoreiTune(){
        return btnRestoreiTune;
    }

    public WebElement getTxtView(){
        return txtView;
    }

    public WebElement getLinkPreference(){
        return linkPreference;
    }

    public LoginPage openLoginPage(){
        btnLogin.click();
        return new LoginPage(iosDriver);
    }

}
