package pages;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

public class SettingNielsenPage extends BasePage {

    public SettingNielsenPage(IOSDriver driver) {
        super(driver);
    }

    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='ABOUT NIELSEN MEASUREMENT']")
    private WebElement nielsenHeader;

    public String getNielsenHeader(){
        return nielsenHeader.getAttribute("label");
    }
}
