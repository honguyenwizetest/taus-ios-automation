package pages.components;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.BasePage;
import pages.utils.UtilMethods;

public class SettingContainer extends BasePage {

    private SettingAutoDeleteDownloadDialog settingAutoDeleteDownloadDialog;

    public SettingContainer(IOSDriver driver) {
        super(driver);
        settingAutoDeleteDownloadDialog = new SettingAutoDeleteDownloadDialog(driver);
    }

    //region Universal Elements

    @iOSFindBy(accessibility = "Version")
    private WebElement menuVersion;// universal

    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Version']/following-sibling::XCUIElementTypeStaticText")
    private WebElement menuVersionNumber;// universal


    @iOSFindBy(accessibility = "Badge Button")
    private WebElement badgeButton;// universal

    //endregion

    //region iPad Elements

    @iOSFindBy(accessibility = "Notification")
    private WebElement menuIpadLinkNotification;// Ipad

    @iOSFindBy(accessibility = "Digital print edition")
    private WebElement menuIpadLinkDigitalEdition;//

    @iOSFindBy(accessibility = "Back")
    private WebElement settingNotificationsBackButton; // Ipad

    @iOSFindBy(accessibility = "Logout")
    private WebElement settingBtnLogout; // Ipad

    @iOSFindBy(accessibility = "Log out")
    private WebElement settingBtnLogoutConfirm; // Ipad



    //endregion

    //region Universal Functions

    public WebElement getmenuVersion(){
        return menuVersion;
    }

    public String getWindowHeader(String windowName){
        String xpath = "//XCUIElementTypeOther[@name='"+windowName+"']";
        WebElement windowHeader = UtilMethods.waitForElementVisible(iosDriver, iosDriver.findElement(By.xpath(xpath)), 10);
        return windowHeader.getAttribute("label");
    }

    public String getVersionNumber(){
        return menuVersionNumber.getText();
    }

    public WebElement getSettingMenuItem(String settingName){
        WebElement settingMenuItem = UtilMethods.waitForElementPresent(iosDriver,By.xpath("//XCUIElementTypeStaticText[@name='"+settingName+"']"),10);
        return settingMenuItem;
    }

    public String getSettingDescription(String settingName){
        WebElement settingDescription = iosDriver.findElement(
                By.xpath("//XCUIElementTypeStaticText[@name='"+settingName+"']/following-sibling::XCUIElementTypeStaticText"));
        return settingDescription.getText();
    }

    public WebElement getSettingSwitchButton(String settingName){
        By bySwitchButton = By.xpath("//XCUIElementTypeStaticText[@name='"+settingName+"']/following-sibling::XCUIElementTypeSwitch");

        if(UtilMethods.waitForElementClickable(iosDriver,bySwitchButton,5)==null)
        {
            UtilMethods.swipeFromUpToBottom(iosDriver);
        }

        return iosDriver.findElement(bySwitchButton);
    }

    public String getSettingSwitchButtonStatus(String settingName){
        return getSettingSwitchButton(settingName).getAttribute("value").equals("1")?"true":"false";
    }

    public WebElement getDownloadBadgeButton(){
        return badgeButton;
    }

    public SettingAutoDeleteDownloadDialog getSettingAutoDeleteDownloadDialog(){
        return settingAutoDeleteDownloadDialog;
    }

    //endregion

    //region iPad Functions

    public boolean isNotificationBackButtonDisplayed(){
        return settingNotificationsBackButton.isDisplayed();
    }

    public void logout(String device){
        settingBtnLogout.click();
        if(device.equalsIgnoreCase("ipad")) {
            settingBtnLogoutConfirm.click();
        }
    }
    //endregion
}
