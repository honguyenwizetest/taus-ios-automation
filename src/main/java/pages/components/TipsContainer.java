package pages.components;

import org.openqa.selenium.WebElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.*;
import pages.BasePage;
import pages.HomePage;
import pages.utils.*;

public class TipsContainer extends BasePage {

    @iOSFindBy(xpath = "//XCUIElementTypePageIndicator/parent::*/XCUIElementTypeScrollView")
    private WebElement tipContents;

    @iOSFindBy(xpath = "//XCUIElementTypePageIndicator")
    private WebElement pageIndicator;

    @iOSFindBy(accessibility = "Next")
    private WebElement btnNext;

    @iOSFindBy(accessibility = "Done")
    private WebElement btnDone;

    public TipsContainer(IOSDriver driver) {
        super(driver);
    }

    public HomePage goThroughTips(){
        if(UtilMethods.isElementVisible(iosDriver,pageIndicator,10)) { // if the walkthrough is shown

            while (!isTheLastTip()) {
                btnNext.click();
            }

            btnDone.click();
        }

        return new HomePage(iosDriver);
    }

    private boolean isTheLastTip(){
        String [] pageInfoPart = pageIndicator.getText().split(" ");
        return pageInfoPart[1].equals(pageInfoPart[pageInfoPart.length-1]);
    }

    public boolean checkTipContentsVisible(){
        return UtilMethods.isElementVisible(iosDriver, tipContents,10);
    }

    public boolean checkPageIndicatorVisible(){
        return UtilMethods.isElementVisible(iosDriver, pageIndicator,10);
    }

    public boolean checkNextTipButtonVisible(){
        return UtilMethods.isElementVisible(iosDriver, btnNext,10);
    }

    public boolean checkDoneTipButtonVisible(){
        return UtilMethods.isElementVisible(iosDriver, btnDone,10);
    }

}
