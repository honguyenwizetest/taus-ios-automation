package pages.components;

import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.WebElement;
import pages.BasePage;

public class SettingAutoDeleteDownloadDialog extends BasePage {

    public SettingAutoDeleteDownloadDialog(IOSDriver driver){
        super(driver);
    }

    public WebElement getAutoDeleteDownloadButton(String buttonName){
        return iosDriver.findElementByAccessibilityId(buttonName);
    }
}
