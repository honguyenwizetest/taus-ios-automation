package pages;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import pages.utils.UtilMethods;

public class SettingPrivacyPolicyPage extends BasePage {

    public SettingPrivacyPolicyPage(IOSDriver driver) {
        super(driver);
    }

    private By byNavBar = By.xpath("//XCUIElementTypeOther[@name='navigation']");

    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Privacy Policy']")
    private WebElement headerPrivacyPolicy;

    @iOSFindBy(xpath = "//XCUIElementTypeOther[@name='Privacy Policy']//following-sibling::XCUIElementTypeOther[1]//XCUIElementTypeStaticText")
    private List<WebElement> privacyPolicyContent;

    public boolean isNavigationBarPresented(){
        return UtilMethods.waitForElementPresent(iosDriver, byNavBar, 10)!=null;
    }

    public String getHeader(){
        return headerPrivacyPolicy.getAttribute("label");
    }

    public ArrayList<String> getPrivacyPolicysContent(){
        ArrayList<String> policyList = new ArrayList<>();

        for(WebElement policy: privacyPolicyContent){
            policyList.add(policy.getText());
        }

        return policyList;
    }

    public boolean isPrivacyPolicyExisting(String privacyPolicy){
        WebElement privacyContent = UtilMethods.waitForElementPresent(iosDriver, By.xpath("//XCUIElementTypeStaticText[@name='"+privacyPolicy+"']"), 10);
        return privacyContent.getAttribute("label").equals(privacyPolicy);
    }
}
