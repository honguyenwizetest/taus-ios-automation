package pages;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.utils.UtilMethods;

public class LoginPage extends BasePage {

    public LoginPage(IOSDriver driver) {
        super(driver);
    }

    @iOSFindBy(accessibility = "Log in to your account")
    private WebElement txtLoginHeader;

    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Email or username']/parent::*/child::XCUIElementTypeTextField")
    private WebElement txtUsername;

    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Password']/parent::*/child::XCUIElementTypeSecureTextField")
    private WebElement txtPassword;

    @iOSFindBy(xpath = "//XCUIElementTypeButton[contains(@name,'Forgot your password?')]")
    private WebElement linkTextForgotPassword;

    @iOSFindBy(accessibility = "LOG IN")
    private WebElement btnLogin;

    @iOSFindBy(accessibility = "Cancel")
    private WebElement btnCancel;

    public WebElement getTxtLoginHeader(){
        return txtLoginHeader;
    }

    public WebElement getTxtUsername(){
        return txtUsername;
    }

    public WebElement getTxtPassword(){
        return txtPassword;
    }

    public WebElement getLinkTextForgotPassword(){
        return linkTextForgotPassword;
    }

    public WebElement getBtnLogin(){
        return btnLogin;
    }

    public WebElement getBtnCancel(){
        return btnCancel;
    }

    public boolean isLoginFormPresent(){
        if(UtilMethods.waitForElementPresent(iosDriver,By.xpath("//XCUIElementTypeStaticText[contains(@name,'Log in to your account')]"),5)==null){
            return false;
        }

        return true;
    }

    public boolean isTextExisting(String text){
        return iosDriver.findElement(By.xpath("//XCUIElementTypeStaticText[contains(@name,'"+text+"')]")).getText().trim().equals(text);
    }

    public void login(String username, String password){
        txtUsername.clear();
        txtUsername.sendKeys(username);
        txtPassword.clear();
        txtPassword.sendKeys(password);

        btnLogin.click();
    }




}
