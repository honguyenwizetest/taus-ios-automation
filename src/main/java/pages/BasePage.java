package pages;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class BasePage {

    protected IOSDriver iosDriver;

    public BasePage(IOSDriver driver){
        this.iosDriver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(iosDriver), this);
    }
}
