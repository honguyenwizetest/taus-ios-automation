package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.*;
import pages.components.SettingContainer;
import pages.components.TipsContainer;
import pages.utils.UtilMethods;

public class HomePage extends BasePage{

    public HomePage(IOSDriver driver) {
        super(driver);

        settingContainer = new SettingContainer(driver);
        tipsContainer = new TipsContainer(driver);
    }

    private SettingContainer settingContainer;
    private TipsContainer tipsContainer;

    @iOSFindBy(accessibility = "MIND GAMES")
    private WebElement sectionMindgameLink;

    @iOSFindBy(accessibility = "TODAY'S PAPER")
    private WebElement sectionTodayPaperLink;

    @iOSFindBy(accessibility = "WISH")
    private WebElement sectionWishLink;

    @iOSFindBy(accessibility = "SAVED STORIES")
    private WebElement sectionSaveStoriesLink;

    @iOSFindBy(accessibility = "Refresh Button")
    private WebElement btnRefresh;

    @iOSFindBy(accessibility = "Settings Button")
    private WebElement btnSetting;

    @iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeStaticText")
    private WebElement imgLogo;

    @iOSFindBy(xpath = "//XCUIElementTypeCollectionView")
    private WebElement articleList;


//    public List<WebElement> getSectionList(){
//        return iosDriver.findElements(By.xpath("//XCUIElementTypeStaticText[@name='SAVED STORIES']/parent::*/parent::*//XCUIElementTypeStaticText"));
//    }

    public WebElement getSectionLinkByName(String sectionName){
        return iosDriver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='MIND GAMES']/parent::*/parent::*//XCUIElementTypeStaticText[@name=\""+sectionName+"\"]"));
    }

    public int totalArticleInSection(){
        return articleList.findElements(By.xpath(".//XCUIElementTypeCell")).size();
    }

    public WebElement getArticleByPosition(int articleNo){
        return articleList.findElement(By.xpath(".//XCUIElementTypeCell["+articleNo+"]"));

    }

    public WebElement getArticleOpenable(int articleNo){
        By byArticleImage = By.xpath("//XCUIElementTypeCollectionView/XCUIElementTypeCell["+articleNo+"]");
        return UtilMethods.waitForElementClickable(iosDriver,byArticleImage,10);
    }

    public SettingContainer getSettingContainer(){
        return settingContainer;
    }

    public TipsContainer getTipsContainer(){
        return tipsContainer;
    }

    public void goToSettingOption(String optionName){
        WebElement settingOption = settingContainer.getSettingMenuItem(optionName);
        if(settingOption != null){
            if(!UtilMethods.isElementClickable(iosDriver,settingOption,5)) { // setting option is presented, but cannot click
                UtilMethods.swipeFromObject2OtherObject(iosDriver,settingContainer.getmenuVersion(), settingOption);
            }
            settingContainer.getSettingMenuItem(optionName).click();
        }
//        else { // iphone: Notifications or Digital print edition -> scroll down to see the component
//            UtilMethods.swipeFromUpToBottom(iosDriver);
//        }
    }

    public WebElement getBtnRefresh(){
        if(UtilMethods.isElementClickable(iosDriver,btnRefresh,10)) {
            return btnRefresh;
        }
        return null; }

    public WebElement getBtnSetting(){
        if(UtilMethods.isElementClickable(iosDriver,btnSetting,10)) {
            return btnSetting;
        }
        return null;
    }

    public WebElement getNavigationBarLogo(){ return imgLogo;}

    public String getSectionText(String sectionName){
        return getSectionLinkByName(sectionName).getText();
    }

    public boolean openSectionByName(String sectionName){
        if(UtilMethods.isElementClickable(iosDriver, getSectionLinkByName(sectionName),30)) {
            getSectionLinkByName(sectionName).click();
            return true;
        }
        return false;
    }

}
