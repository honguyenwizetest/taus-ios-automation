package wsModel;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public class WSMethods {

    public static void main(String[] args) throws Exception{
        String json;
        String urlArticle = "https://www.theaustraliansit.com.au/apps/taus/theaters/collections?device_type=tablet&screen_ids=45b2dfed68d8be74869740702691c6e7&secret_key=rhsr62yc8nc5g9sgmnn4tdp9";
        json = getjSonFromRequest(urlArticle);
        Gson gson = new Gson();
        Article article = gson.fromJson(json, Article.class);

        System.out.println(article.toString());


        String urlCollection = "https://www.theaustraliansit.com.au/apps/taus/theaters/collections?device_type=tablet&screen_ids=f4b64e6403238f3aef150c9dc20d8fd5&secret_key=rhsr62yc8nc5g9sgmnn4tdp9";
        json = getjSonFromRequest(urlCollection);
        gson = new Gson();
        Collection collection = gson.fromJson(json, Collection.class);

        System.out.println(article.toString());

    }

    public static String getjSonFromRequest(String url) throws Exception{
        StringBuilder builder = new StringBuilder();
        try (CloseableHttpClient client = HttpClientBuilder.create().build()) {

            HttpGet request = new HttpGet(url);

//            Map<String, String> headers = getRequestHeaders();
//            headers.forEach((k,v)-> request.setHeader(k,v));
            request.setHeader("X-BeQuietPlease", "WeAreHuntingMobileCert");

            HttpResponse response = client.execute(request);

            BufferedReader bufReader = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));

            String line;

            while ((line = bufReader.readLine()) != null) {
                builder.append(line);
                builder.append(System.lineSeparator());
            }
        }
        // System.out.println(builder.toString());
        return builder.toString();
    }

    public static Map<String,String> getRequestHeaders(){
        ResourceBundle resource = ResourceBundle.getBundle(System.getProperty("config.file"));
        Map<String, String> headers = new HashMap<>();
        headers.put("X-BeQuietPlease", resource.getString("ws.cert"));
//        headers.put("X-Edition-Date", resource.getString("ws.editionDate"));

        return headers;
    }
}

