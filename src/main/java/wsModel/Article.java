package wsModel;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class Article {
    String type;
    String id;
    String baseUrl;
    String[] screenIds;
    List<Screen> screens;
}

@Setter
@Getter
 class Screen{
     String id;
     List<Frame> frames;

}

@Setter
@Getter
class Frame{
    String type;
    Title title;
    Name name;
    Body body;

}

@Setter
@Getter
class Title {
    String text;
}

@Setter
@Getter
class Name {
    String text;
}

@Setter
@Getter
class Body {
    String text;
}



