package wsModel;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Collection {
    String[] screenIds;
}
