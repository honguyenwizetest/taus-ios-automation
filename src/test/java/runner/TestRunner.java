package runner;

import org.testng.annotations.Parameters;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@CucumberOptions(
        tags = {"@test"},
//        tags = {"@ipad"},
//        tags = {"@iphone"},
        plugin = {"pretty" , "html:target/cucumerReport"},
        features = "./src/test/java/features",
        glue={"stepdefs"}
)
public class TestRunner {
    private TestNGCucumberRunner testNGCucumberRunner;

    @Parameters({"deviceName", "platformVersion"})
    @BeforeClass(alwaysRun = true)
    public void setUpClass(String deviceName, String platformVersion) throws Exception {
        System.setProperty("deviceName", deviceName);
        System.setProperty("platformVersion", platformVersion);
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
    }

    @Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
    public void feature(CucumberFeatureWrapper cucumberFeature) {
        testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
    }

    @DataProvider
    public Object[][] features() {
        return testNGCucumberRunner.provideFeatures();
    }

    @AfterClass(alwaysRun = true)
    public void tearDownClass() throws Exception {
        testNGCucumberRunner.finish();
    }

}