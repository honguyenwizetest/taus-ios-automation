package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import org.testng.annotations.*;

@CucumberOptions(
        tags = {"@loginsuccessfully"},
        plugin = {"pretty" , "html:target/cucumerReport"},
        features = "./src/test/java/features",
        glue={"stepdefs"}
)
public class TestRunnerInit {
    private TestNGCucumberRunner testNGCucumberRunner;

    @Parameters({"deviceName", "platformVersion"})
    @BeforeClass(alwaysRun = true)
    public void setUpClass(String deviceName, String platformVersion) throws Exception {
        System.setProperty("deviceName", deviceName);
        System.setProperty("platformVersion", platformVersion);
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
    }

    @Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
    public void feature(CucumberFeatureWrapper cucumberFeature) {
        testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
    }

    @DataProvider
    public Object[][] features() {
        return testNGCucumberRunner.provideFeatures();
    }

    @AfterClass(alwaysRun = true)
    public void tearDownClass() throws Exception {
        testNGCucumberRunner.finish();
    }

}