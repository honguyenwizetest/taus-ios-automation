package stepdefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import org.testng.Assert;
import pages.LoginPage;
import pages.SubscribePage;

public class LoginStepDef extends BaseStepDefs {
    private SubscribePage subscribePage;
    private LoginPage loginPage;

    @And("^I see a subscribe page$")
    public void initSubscribePage(){
        subscribePage = new SubscribePage(getDriver());
    }

    @And("^I see '(LOG IN|SUBSCRIBE NOW)' button in the subscribe page$")
    public void checkButtonOnSubscribePage(String buttonName){
        if(buttonName.equals("LOG IN")){
            Assert.assertTrue(subscribePage.getBtnLogin().isDisplayed(), "Button " + buttonName + " is not found in the subscribe Page");
        }else {
            Assert.assertTrue( subscribePage.getBtnSubscribe().isDisplayed(),"Button " + buttonName + " is not found in the subscribe Page");
        }
    }

    @And("^I click on '(LOG IN|SUBSCRIBE NOW)' button in in the subscribe page$")
    public void openLoginPage(String buttonName){
        if(buttonName.equals("LOG IN")){
            loginPage = subscribePage.openLoginPage();
        }else {
            subscribePage.getBtnSubscribe().click();
        }
    }

    @And("^I see '(.+)' link in the subscribe page$")
    public void checkLinkButtonOnSubscribePage(String expectedLinkText){
        Assert.assertEquals(expectedLinkText, subscribePage.getBtnRestoreiTune().getText());
    }

    @And("^I see the link '(.+)' in the subscribe page$")
    public void checkLinkTextOnSubscribePage(String expectedText){
        Assert.assertEquals(subscribePage.getLinkPreference().getText(),expectedText);
    }

    @And("^I see the text '(.+)' in the subscribe page$")
    public void checkTextOnSubscribePage(String expectedText){
        Assert.assertTrue(subscribePage.isSubscribeTextExisting(expectedText), "No text of " + expectedText + " found.");
    }

    @And("^I see the text view '(.+)' in the subscribe page$")
    public void checkTextViewOnSubscribePage(String expectedText){
        Assert.assertTrue(subscribePage.getTxtView().getText().contains(expectedText), "No text of " + expectedText + " found.");
    }

    @And("^I see '(LOG IN|Cancel)' button in the login page$")
    public void checkButtonInLoginPage(String buttonName){
        if(buttonName.equals("LOG IN")){
            Assert.assertTrue(loginPage.getBtnLogin().isDisplayed(), "Button " + buttonName + " is not found in Login Page");
        }else {
            Assert.assertTrue(loginPage.getBtnCancel().isDisplayed(), "Button " + buttonName + " is not found in Login Page");
        }
    }

    @And("^I see the text '(.+)' in the login page$")
    public void checkTextOnLoginPage(String expectedText){
        Assert.assertTrue(loginPage.isTextExisting(expectedText), "No text of " + expectedText + " found.");
    }

    @And("^I see '(.+)' link in the login page$")
    public void checkLinkButtonOnLoginPage(String expectedLinkText){
        Assert.assertEquals(expectedLinkText, loginPage.getLinkTextForgotPassword().getText().trim());
    }


    @When("^I login with '(.+)' username and '(.+)' password$")
    public void login(String username, String pass){
        if(username.equals("valid")){
            username = resource.getString("auth.username");
        }
        if(pass.equals("valid")){
            pass = resource.getString("auth.password");
        }

        loginPage.login(username,pass);
    }

    @And("^I see login page is '(opened|closed)'$")
    public void checkLoginFormShown(String status){
        Assert.assertTrue(status.equals("opened")== loginPage.isLoginFormPresent(), "Login form is expected to be " + status);
    }


}
