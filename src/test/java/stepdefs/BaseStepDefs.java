package stepdefs;

import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.ios.IOSDriver;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;


public class BaseStepDefs {

    private static IOSDriver driver;
    static ResourceBundle resource = ResourceBundle.getBundle(System.getProperty("config.file"));

    public IOSDriver startDriver(boolean isNoReset) throws Exception{

        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();

        desiredCapabilities.setCapability("deviceName", System.getProperty("deviceName"));
        desiredCapabilities.setCapability("platformVersion", System.getProperty("platformVersion"));
        desiredCapabilities.setCapability("platformName", resource.getString("capabilities.platformName"));
        desiredCapabilities.setCapability("app", resource.getString("capabilities.app"));
        desiredCapabilities.setCapability("noReset", isNoReset);
        desiredCapabilities.setCapability("autoAcceptAlerts", resource.getString("capabilities.autoAcceptAlerts"));

        URL remoteUrl = new URL(resource.getString("capabilities.remoteUrl"));

        driver = new IOSDriver(remoteUrl, desiredCapabilities);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return driver;
    }

    public IOSDriver getDriver(){
        return driver;
    }

}
