package stepdefs;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pages.HomePage;
import pages.utils.UtilMethods;

import java.util.Arrays;
import java.util.List;

public class HomePageStepDefs extends BaseStepDefs{

    private HomePage homePage;

    @When("I am on the home page")
    public void openHomePage(){

        homePage = new HomePage(getDriver());
    }

    @And("^I see articles in the configured '(.+)' section$")
    public void openSection(String configuredSection){
        List<String> configuredSectionName  = Arrays.asList(resource.getString(configuredSection).split(","));

        for(String sectionName : configuredSectionName) {
            Assert.assertTrue(homePage.openSectionByName(sectionName), "Cannot open the section ");
            Assert.assertTrue(homePage.totalArticleInSection() > 0, "No article found in the section");
        }
    }

    @And("^I see '(Refresh|Settings)' button in the navigation bar$")
    public void checkNavigationBar(String buttonName){
        WebElement navButton = buttonName.equals("refresh")?homePage.getBtnRefresh():homePage.getBtnSetting();
        Assert.assertTrue(navButton.isDisplayed(), "No " + buttonName + " button found in the navigation bar");
    }

    @And("^I see a logo in the navigation bar$")
    public void checkNavigationBar(){
        Assert.assertTrue(homePage.getNavigationBarLogo().isDisplayed(), "No logo found in the navigation bar");
    }

    @When("^I click on '(Refresh|Settings)' button in the navigation bar$")
    public void clickNavigationButton(String buttonName){
        WebElement navButton = buttonName.equals("refresh")?homePage.getBtnRefresh():homePage.getBtnSetting();
        navButton.click();
    }

    @And("I go to '(.+)' on the setting window")
    public void gotoSettingWindow(String optionName){
        homePage.goToSettingOption(optionName);
    }


    @And("I click on '(Logout|Rewards|My Account)' button in the account page for '(.+)'")
    public void logout(String buttonName, String device){
        if(buttonName.equals("Logout")) {
            homePage.getSettingContainer().logout(device);
        }
    }

    @And("I see the walk through dialog displayed")
    public void checkTipsDialog(){
        Assert.assertTrue(homePage.getTipsContainer().checkTipContentsVisible(),"No tip in the walk through dialog found");
    }

    @And("I see a page indicator in the walk through dialog")
    public void checkPageIndicatorInTipsDialog(){
        Assert.assertTrue(homePage.getTipsContainer().checkPageIndicatorVisible(),"No page indicator in the walk through dialog found");
    }

    @And("I see '(Next|Done)' button in the walk through dialog")
    public void checkButtonInTipsDialog(String buttonName){
        boolean isButtonVisible = buttonName.equals("Next")?
                homePage.getTipsContainer().checkNextTipButtonVisible():
                homePage.getTipsContainer().checkDoneTipButtonVisible();
        Assert.assertTrue(isButtonVisible,"No " + buttonName + " button in the walk through dialog found");
    }

    @Then("^I see the home page contains the configured '(.+)'$")
    public void checkSections(String configuredSection){
        List<String> expectedSectionNames  = Arrays.asList(resource.getString(configuredSection).split(","));
        //List<String> expectedSectionNames = expectedSections.asList(String.class);
        for (String expectedSectionName: expectedSectionNames){
            String actualSectionName = homePage.getSectionText(expectedSectionName);
            Assert.assertEquals(expectedSectionName,actualSectionName);
        }
    }

    @And("I click on the article No. '(.+)'")
    public void open1stArticle(int articleNo){
        WebElement articleToOpen = homePage.getArticleOpenable(articleNo);
        if(articleToOpen!=null) {
            UtilMethods.tapElement(getDriver(), articleToOpen);
        }
        else {
            Assert.fail("The article without image No: " +articleNo + " is not able to open.");
        }
    }

}
