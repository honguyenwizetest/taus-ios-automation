package stepdefs;

import cucumber.api.java.en.When;
import pages.Article;

public class ArticleStepDefs extends BaseStepDefs{

    private Article articlePage;

    @When("I am on the article page")
    public void openArticlePage(){

        articlePage = new Article(getDriver());
    }

    @When("I receive the data from the UI")
    public void receiveDataFromUI(){

        String articleTitle = articlePage.getArticleText();
    }

}
