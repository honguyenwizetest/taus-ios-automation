package stepdefs;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pages.*;
import pages.components.TipsContainer;
import pages.utils.UtilMethods;

import java.util.List;

public class SettingStepDefs extends BaseStepDefs{

    private IOSDriver sdriver;
    private HomePage homePage;
    private TipsContainer tipsContainer ;
    private SettingHelpPage settingHelpPage;
    private SettingPrivacyPolicyPage settingPrivacyPolicyPage;
    private SettingNielsenPage settingNielsenPage;

    //@Parameters("deviceName")
    @Given("^I open the app$")
    public void launchApp() throws Exception {
        sdriver = startDriver(true);
        initPages();
    }

    @Given("^I launch the app$")
    public void launchAppAt1stTime() throws Exception {
        sdriver = startDriver(false);
        initPages();
    }

    private void initPages(){
        tipsContainer = new TipsContainer(sdriver);
        settingHelpPage = new SettingHelpPage(sdriver);
        settingPrivacyPolicyPage = new SettingPrivacyPolicyPage(sdriver);
        settingNielsenPage = new SettingNielsenPage(sdriver);
        homePage = new HomePage(getDriver());
    }

    @And("^I close the app$")
    public void tearDown() {
        getDriver().quit();
    }

    @When("^I go through Walkthrough dialog$")
    public void keepClickingWalkthroughButton() {
        tipsContainer.goThroughTips();
    }

    @When("^I see the help page displayed with banner '(.+)'$")
    public void checkHelpBanner(String banner) {
        Assert.assertEquals(banner, settingHelpPage.getBanner());
    }

    @When("^I see the Nielsen page displayed with banner '(.+)'$")
    public void checkNielsenBanner(String banner) {
        Assert.assertEquals(banner, settingNielsenPage.getNielsenHeader());
    }

    @When("^I see the Privacy Policy page displayed with header '(.+)'$")
    public void checkPrivacyPolicyHeader(String header){
        Assert.assertEquals(header, settingPrivacyPolicyPage.getHeader() ,"No header found in Privacy Policy Page");
    }

    @When("^I see the help page contains Search button$")
    public void checkHelpSearchField() {
        Assert.assertTrue(settingHelpPage.checkSearchField(),"No Search field found in Help page");
    }

    @When("^I see the Privacy Policy page contains navigation bar$")
    public void checkNavigationBarInPrivacyPage() {
        Assert.assertTrue(settingPrivacyPolicyPage.isNavigationBarPresented(),"No Navigation Bar found in Privacy Policy Page");
    }

    @When("^I see the version number in the settings$")
    public void checkVersionNumber() {
        Assert.assertTrue(!homePage.getSettingContainer().getVersionNumber().isEmpty(),"No Version Number found in Setting Window");
    }

    @Then("^I see the Privacy Policy page contains the following content$")
    public void checkPrivacyPolicyContent(DataTable expectedPrivacyPolicyContent){
        List<String> expectedPolicyContent= expectedPrivacyPolicyContent.asList(String.class);
        List<String> actualPolicyContent = settingPrivacyPolicyPage.getPrivacyPolicysContent();

        Assert.assertEquals(expectedPolicyContent,actualPolicyContent);
    }


    @Then("^I see the help page contains the following content details$")
    public void checkPrivacyPolicyContentDetails(DataTable expectedContentDetails){
        List<String> expectedPolicyContentDetails = expectedContentDetails.asList(String.class);

        for(String expectedContentDetail: expectedPolicyContentDetails){
            Assert.assertTrue(settingPrivacyPolicyPage.isPrivacyPolicyExisting(expectedContentDetail),
                    "No PrivacyPolicyContent '"+expectedContentDetail+"'found");
        }
    }

    @When("^I see the help page contains question '(.+)'$")
    public void checkHelpQuestion(String question) {
        Assert.assertTrue(settingHelpPage.getHelpQuestions().contains(question));
    }

    @Then("^I see a window with a header '(Settings|Notifications)'$")
    public void checkSettingMenu(String settingHeader){
        Assert.assertEquals(homePage.getSettingContainer().getWindowHeader(settingHeader), settingHeader);
    }

    @Then("^I see a back button displayed in the notification window$")
    public void checkSettingBackButton (){
        Assert.assertTrue(homePage.getSettingContainer().isNotificationBackButtonDisplayed(), "No Back button in notification window found");
    }


    @Then("I see the setting switch button in '(.+)' with '(true|false)'$")
    public void checkSettingMenu(String settingItem, String status){
        Assert.assertEquals(homePage.getSettingContainer().getSettingSwitchButtonStatus(settingItem), status);
    }

    @And("^I see the Download Badge Button shows '(.+)'$")
    public void checkAutoDeleteOption(String dayNo){
        if(!UtilMethods.isElementVisible(sdriver,homePage.getSettingContainer().getDownloadBadgeButton(),5)){
            UtilMethods.swipeFromUpToBottom(sdriver);
        }

        Assert.assertEquals(homePage.getSettingContainer().getDownloadBadgeButton().getText().toLowerCase(), dayNo.toLowerCase());
    }

    @Then("I click on Download Badge Button$")
    public void clickDownloadBadgeButton(){
        homePage.getSettingContainer().getDownloadBadgeButton().click();
    }

//    @Then("I verify if the Download Badge dialog containing option '(.+)'$")
//    public void verifyDownloadBadgeButton(String expectedOption){
//        homePage.getSettingContainer().getDownloadBadgeButton().click();
//
//        WebElement option = homePage.getSettingContainer().getSettingAutoDeleteDownloadDialog().
//                getAutoDeleteDownloadButton(expectedOption);
//
//        String actualOption = option.getAttribute("label");
//
//        Assert.assertEquals(actualOption, expectedOption);
//
//        while (true){
//            if(!UtilMethods.isElementVisible(sdriver,option,3))
//                break;
//        }
//    }

    @Then("I select '(.+)' in the dialog Auto-Delete downloads$")
    public void checkDownloadBadgeButton(String expectedOption){
        UtilMethods.swipeFromUpToBottom(sdriver);
        homePage.getSettingContainer().getDownloadBadgeButton().click();

        WebElement option = homePage.getSettingContainer().getSettingAutoDeleteDownloadDialog().
                getAutoDeleteDownloadButton(expectedOption);
        option.click();
        while (true){
            if(!UtilMethods.isElementVisible(sdriver,option,3))
                break;
        }
    }

    @Then("^I click on switch button in the setting '(.+)'$")
    public void switchSettingButton (String settingItem){
        WebElement switchButton = homePage.getSettingContainer().getSettingSwitchButton(settingItem);
//
//        if(!homePage.getSettingContainer().isSwitchButtonClickable(settingItem)){
//            UtilMethods.swipeFromUpToBottom(sdriver);
//        }

        switchButton.click();
    }

    @Then("I see the setting '(.+)' with description '(.+)'$")
    public void checkSettingDescription(String settingItem, String settingDescription){
        Assert.assertEquals(homePage.getSettingContainer().getSettingDescription(settingItem), settingDescription);
    }

    @Then("^I see the setting window contains the following options$")
    public void checkSettingMenu(DataTable expectedSettingMenu){
        List<String> expectedSettingMenuNames = expectedSettingMenu.asList(String.class);

        for (String expectedMenuName: expectedSettingMenuNames){
            String actualSettingMenuName  = homePage.getSettingContainer().getSettingMenuItem(expectedMenuName).getText();
            Assert.assertEquals(expectedMenuName,actualSettingMenuName);
        }
    }
}
