@login
Feature: Login feature

  @iphone
  Scenario: I logout my iphone successfully
    Given I open the app
    Then I am on the home page
    When I click on 'Settings' button in the navigation bar
    And I click on 'Logout' button in the account page for 'iPhone'

  @ipad
  Scenario: I logout my ipad successfully
    Given I open the app
    Then I am on the home page
    When I click on 'Settings' button in the navigation bar
    And I go to 'Account' on the setting window
    And I click on 'Logout' button in the account page for 'iPad'

  @ipad
  @iphone
  Scenario: I check Subscription
    Given I open the app
    And I am on the home page
    When I click on the article No. '1'
      Then I see a subscribe page
      And I see the text 'Already a subscriber?' in the subscribe page
      And I see 'LOG IN' button in the subscribe page
      And I see the text 'Subscribe now to enjoy The Australian app' in the subscribe page
      And I see the text 'Subscribe for a monthly recurring subscription of $33.99 per month and receive your first month FREE.' in the subscribe page
      And I see 'SUBSCRIBE NOW' button in the subscribe page
      And I see 'Restore iTunes subscription' link in the subscribe page
      And I see the text 'Subscribe now to enjoy The Australian app' in the subscribe page
      And I see the text view 'Payment will be charged to iTunes Account at confirmation of purchase. Subscription automatically renews unless auto-renew is turned off at least 24-hours before the end of the current period. Account will be charged for renewal within 24-hours prior to the end of the current period, and identify the cost of the renewal. Subscriptions may be managed by the user and auto-renewal may be turned off by going to the user's Account Settings after purchase' in the subscribe page
      And I see the link 'preferences.news.com.au' in the subscribe page

  @ipad
  @iphone
  Scenario: I check login page
    Given I open the app
    And I am on the home page
    When I click on the article No. '1'
    Then I see a subscribe page
    And I see 'LOG IN' button in the subscribe page
    When I click on 'LOG IN' button in in the subscribe page
    Then I see login page is 'opened'
    And I see the text 'Log in to your account' in the login page
    And I see the text 'Email or username' in the login page
    And I see the text 'Password' in the login page
    And I see 'Forgot your password?' link in the login page
    And I see 'LOG IN' button in the login page
    And I see 'Cancel' button in the login page

  @ipad
  @iphone
  Scenario: I login unsuccessfully
    Given I open the app
    And I am on the home page
    When I click on the article No. '1'
    Then I see a subscribe page
    And I see 'LOG IN' button in the subscribe page
    When I click on 'LOG IN' button in in the subscribe page
    Then I see login page is 'opened'
    When I login with 'unknownUser' username and 'unknownPass' password
    Then I see login page is 'opened'
    When I login with 'valid' username and ' ' password
    Then I see login page is 'opened'
    When I login with ' ' username and 'valid' password
    Then I see login page is 'opened'

  @loginsuccessfully
  Scenario: I login successfully
    Given I launch the app
    And I go through Walkthrough dialog
    And I open the app
    And I am on the home page
    When I click on the article No. '1'
    Then I see a subscribe page
    And I see 'LOG IN' button in the subscribe page
    When I click on 'LOG IN' button in in the subscribe page
    Then I see login page is 'opened'
    When I login with 'valid' username and 'valid' password
    Then I see login page is 'closed'