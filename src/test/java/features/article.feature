@article
@ipad
@iphone
Feature: Article feature
  Background:
    Given I open the app

  Scenario: I check homepage components
    And I am on the home page
    When I click on the article No. '1'
    Then I am on the article page
    And I see 'Refresh' button in the navigation bar
    And I see 'Settings' button in the navigation bar
    And I see a logo in the navigation bar
    And I receive the data from the UI
#    And I see the article title is the same with the ws response
#    And I see the article author is the same with the ws response
#    And I see the article paragraphs is the same with the ws response
#    And I see the article divider is the same with the ws response