@homepage
@ipad
@iphone
Feature: Homepage feature
  Background:
    Given I open the app

  Scenario: I check homepage components
    When I am on the home page
    Then I see 'Refresh' button in the navigation bar
    And I see 'Settings' button in the navigation bar
    And I see a logo in the navigation bar
    Then I see the home page contains the configured 'sections'
    And I close the app

  Scenario: I check every section in the home page
    When I am on the home page
    Then I see articles in the configured 'sections' section
    And I close the app
