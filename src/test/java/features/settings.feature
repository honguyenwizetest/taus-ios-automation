@setting
Feature: Settings feature

  @ipad
  Scenario: I check Setting menu on iPad
    Given I open the app
    When I am on the home page
    And I click on 'Settings' button in the navigation bar
    Then I see a window with a header 'Settings'
    And I see the setting window contains the following options
      |Read Offline|Tips|Help|Privacy Policy|Rate The Australian|Version|Notification|Digital print edition|Nielsen Analytics|

  @iphone
  Scenario: I check Setting menu on iPhone
    Given I open the app
    When I am on the home page
    And I click on 'Settings' button in the navigation bar
    Then I see a window with a header 'Settings'
    And I see the setting window contains the following options
      |Read Offline|Tips|Help|Privacy Policy|Rate The Australian|Version|Edition|Briefing|Breaking News and Top Stories|Enable highlight|Allow download on Wi-Fi only|Auto-Delete downloads after|Nielsen Analytics|

    @ipad
    @iphone
  Scenario: I check option Tips in Settings
    Given I open the app
    When I am on the home page
    And I click on 'Settings' button in the navigation bar
    And I go to 'Tips' on the setting window
    Then I see the walk through dialog displayed
    And I see a page indicator in the walk through dialog
    And I see 'Next' button in the walk through dialog
    When I go through Walkthrough dialog

 @ipad
 @iphone
  Scenario: I check option Help in Settings
   Given I open the app
    When I am on the home page
    And I click on 'Settings' button in the navigation bar
    And I go to 'Help' on the setting window
    Then I see the help page displayed with banner 'iOS'
    And I see the help page contains Search button
    And I see the help page contains question 'How to setup The Australian app (iPad, iPhone)'
    And I see the help page contains question 'Why do I need to provide additional information after I sign up for my iTunes subscription?'

  @ipad
  @iphone
  Scenario: I check option Privacy Policy in Settings
    Given I open the app
    When I am on the home page
    And I click on 'Settings' button in the navigation bar
    And I go to 'Privacy Policy' on the setting window
    Then I see the Privacy Policy page displayed with header 'Privacy Policy'
    And I see the Privacy Policy page contains navigation bar
    And I see the Privacy Policy page contains the following content
      |1. INTRODUCTION|
      |2. WHAT INFORMATION DO WE COLLECT?|
      |3. HOW DO WE USE YOUR INFORMATION?|
      |4. HOW DO WE PROTECT YOUR INFORMATION?|
      |5. HOW CAN YOU ACCESS YOUR INFORMATION?|
      |6. HOW CAN YOU OPT OUT?|
      |7. OTHER IMPORTANT INFORMATION FOR YOU|
      |8. HOW YOU CAN CONTACT US|
      |9. LIST OF INCLUDED COMPANIES|

    And I see the help page contains the following content details
      |1. INTRODUCTION|
      |2. WHAT INFORMATION DO WE COLLECT?|
      |3. HOW DO WE USE YOUR INFORMATION?|
      |4. HOW DO WE PROTECT YOUR INFORMATION?|
      |5. HOW CAN YOU ACCESS YOUR INFORMATION?|
      |6. HOW CAN YOU OPT OUT?|
      |7. OTHER IMPORTANT INFORMATION FOR YOU|
      |8. HOW YOU CAN CONTACT US|
      |9. LIST OF INCLUDED COMPANIES|

  @ipad
  @iphone
  Scenario: I check option Rate The Australian in Settings
    Given I open the app
    When I am on the home page
    And I click on 'Settings' button in the navigation bar
    And I go to 'Rate The Australian' on the setting window
    # Then I see the appstore shows The Australian app # => Cannot automate with simulator

  @ipad
  @iphone
  Scenario: I check option Version in Settings
    Given I open the app
    When I am on the home page
    And I click on 'Settings' button in the navigation bar
    Then I see the version number in the settings

  @iphone
  @ipad
  @test
  Scenario: I check option Notification in Settings
    Given I open the app
    When I am on the home page
    And I click on 'Settings' button in the navigation bar
    And I go to 'Notification' on the setting window
    And I see the setting window contains the following options
      |Edition|Briefing|Breaking News and Top Stories|
    And I see the setting switch button in 'Edition' with 'true'
    And I see the setting 'Edition' with description 'Start the day with Page One and your favourite sections, Monday-Saturday at 6.30am'
    When I click on switch button in the setting 'Edition'
    Then I see the setting switch button in 'Edition' with 'false'
    When I click on switch button in the setting 'Edition'
    Then I see the setting switch button in 'Edition' with 'true'

    And I see the setting switch button in 'Briefing' with 'true'
    And I see the setting 'Briefing' with description 'The stories you need to know about, Monday-Friday at 7.30am, Noon and 5pm'
    When I click on switch button in the setting 'Briefing'
    Then I see the setting switch button in 'Briefing' with 'false'
    When I click on switch button in the setting 'Briefing'
    Then I see the setting switch button in 'Briefing' with 'true'

    And I see the setting switch button in 'Breaking News and Top Stories' with 'true'
    And I see the setting 'Breaking News and Top Stories' with description 'Breaking news stories and editor's picks during the day'
    When I click on switch button in the setting 'Breaking News and Top Stories'
    Then I see the setting switch button in 'Breaking News and Top Stories' with 'false'
    When I click on switch button in the setting 'Breaking News and Top Stories'
    Then I see the setting switch button in 'Breaking News and Top Stories' with 'true'


  @iphone
  @ipad
  Scenario: I check option Digital print edition in Settings
    Given I open the app
    When I am on the home page
    And I click on 'Settings' button in the navigation bar
    And I go to 'Digital print edition' on the setting window
    And I see the setting window contains the following options
      |Enable highlight|Allow download on Wi-Fi only|Auto-Delete downloads after|
    And I see the setting switch button in 'Enable highlight' with 'true'
    And I see the setting 'Enable highlight' with description 'Use this setting if you wish to highlight the headline'
    And I see the setting switch button in 'Allow download on Wi-Fi only' with 'false'
#    And I see the Download Badge Button shows '14 days'

    When I click on switch button in the setting 'Enable highlight'
    Then I see the setting switch button in 'Enable highlight' with 'false'
    When I click on switch button in the setting 'Allow download on Wi-Fi only'
    Then I see the setting switch button in 'Allow download on Wi-Fi only' with 'true'

    When I click on switch button in the setting 'Enable highlight'
    Then I see the setting switch button in 'Enable highlight' with 'true'
    When I click on switch button in the setting 'Allow download on Wi-Fi only'
    Then I see the setting switch button in 'Allow download on Wi-Fi only' with 'false'

  @iphone
  @ipad
  Scenario: I check Auto-Delete download after popup
    Given I open the app
    When I am on the home page
    And I click on 'Settings' button in the navigation bar
    And I go to 'Digital print edition' on the setting window
    Then I see the Download Badge Button shows '14 days'
    When I select '3 Days' in the dialog Auto-Delete downloads
    Then I see the Download Badge Button shows '3 days'
    When I select '7 Days' in the dialog Auto-Delete downloads
    Then I see the Download Badge Button shows '7 days'
    When I select '21 Days' in the dialog Auto-Delete downloads
    Then I see the Download Badge Button shows '21 days'
    When I select '14 Days' in the dialog Auto-Delete downloads
    Then I see the Download Badge Button shows '14 days'
    When I select 'Cancel' in the dialog Auto-Delete downloads
    Then I see the Download Badge Button shows '14 days'

  @iphone
  @ipad
  Scenario: I check option Nielsen Analytics in Settings
    Given I open the app
    When I am on the home page
    And I click on 'Settings' button in the navigation bar
    And I go to 'Nielsen Analytics' on the setting window
    Then I see the Nielsen page displayed with banner 'ABOUT NIELSEN MEASUREMENT'